#![crate_type = "staticlib"]
#![feature(lang_items)]
#![no_std]
#![no_builtins]


extern crate cortex_m_rt as rt;
extern crate nb;
extern crate nrf52_dk_bsp as dk;
extern crate panic_semihosting;

#[no_mangle]
pub extern "C" fn square(input: i32) -> i32 {
    input * input
}

